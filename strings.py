str_1 = "it is my string 1"
print(str_1)

str_2 = 'it is my string 2'
print(str_2)

str_3 = 'it is my string 3 with "double quotes" inside it'
print(str_3)

str_4 = """It
is
multiline
string
4"""
print(str_4)
